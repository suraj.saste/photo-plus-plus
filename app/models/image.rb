class Image < ApplicationRecord
  validates :name, presence: true
  validates :picture, presence: true
  belongs_to :user
  mount_uploader :picture, PictureUploader
  validate :picture_size

  def get_user_email
    user.email
  end

  private

  def picture_size
    errors.add(:picture, "should be less than 4MB") if picture.size > 4.megabytes
  end
end
